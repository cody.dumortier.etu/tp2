export default class Component {
	tagName;
	children;
	attribute;
	constructor(tagName, attribute, children) {
		this.tagName = tagName;
		this.children = children;
		this.attribute = attribute;
	}
	render() {
		if (!this.children && !this.attribute) {
			return `<${this.tagName} />`;
		} else if (!this.children && this.attribute !== null) {
			return `<${this.tagName} ${this.renderAttribute(this.attribute)} />`;
		} else if (this.children !== null && !this.attribute) {
			return `<${this.tagName}>${this.renderChildren(this.children)}</${
				this.tagName
			}>`;
		} else {
			return `<${this.tagName} ${this.renderAttribute(this.attribute)}>
			${this.renderChildren(this.children)}</${this.tagName}>`;
		}
	}

	renderChildren(children) {
		let res;
		if (children instanceof Array) {
			for (let i = 0; i < children.length; i++) {
				res += children[i];
			}
		} else {
			return children;
		}
	}

	renderAttribute({ name, value }) {
		return `${name}="${value}"`;
	}
}
