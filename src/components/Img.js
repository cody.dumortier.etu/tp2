import Component from './Component.js';

export default class Img extends Component {
	constructor(sourceImg) {
		super(`img`, { name: 'src', value: sourceImg });
	}
}
