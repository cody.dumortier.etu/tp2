import Component from './components/Component.js';

import data from './data.js';

import Img from './components/Img.js';

const title = new Component('h1', null, ['La', ' ', 'carte']);

console.log(title.renderChildren());
document.querySelector('.pageTitle').innerHTML = title.render();
